#include "TimerOne.h"
#include <math.h>
#include <stdlib.h>
//#include <time.h>

#define LED_PIN   13

#define T1_PIN    2
#define T2_PIN    3
#define T3_PIN    4

#define LF_PIN    9
#define L1_PIN    5
#define L2_PIN    6
#define L3_PIN    11

#define POT_PIN   A0

#define SEED_PIN  A5

#define USEC_IN_SEC 1000000
#define SCHEDULER_PERIOD_USEC 10000
#define NUM_OF_TASKS 2
#define CYCLES_PER_SECOND (USEC_IN_SEC / (float)SCHEDULER_PERIOD_USEC / NUM_OF_TASKS)

#define MASK_GAME_RUNNING    0x01
#define MASK_SHOWING_WINNER  0x02
#define MASK_BALL_PASSED     0x04
#define MASK_PLAYER_1        0x08
#define MASK_PLAYER_2        0x10
#define MASK_STOP_WINNER     0x20

#define SIMULATE_GAME   0
#define LED_THEME       0
#define DEBUG_PRINTS    0

#define WIN_BLINK_RESET_VAL   40
#define LF_BLINK_RESET_VAL    30
#define LF_INDICATOR_VAL      30

#define DEBOUNCE_TIME_MS        50
#define SHOWING_WINNER_TIME_S   2


// Global speed - dictates passing frequency
float game_speed = 1.0;
// Task #1 (gameplay) call counter - associated with passing frequency
uint32_t cycle_counter = 0;

// Global program state
uint32_t prog_state = 0;

// Global button states
uint32_t t1, t2, t3;

// PWM (analogWrite) intensity counters
int32_t win_blink_counter = WIN_BLINK_RESET_VAL;
int32_t win_counter_invert = 1;
int32_t lf_blink_counter = LF_BLINK_RESET_VAL;
int8_t lf_counter_invert = 1;

// Global scoreboard
int player_1_wins = 0;
int player_2_wins = 0;
// has to be 'int', not (u)int32_t, otherwise compiler goes mad
//uint32_t player_1_wins = 0;
//uint32_t player_2_wins = 0;

// Global statistics
uint32_t pass_counter;


/*
 * SHCEDULER
 */
void scheduler(){
    // scheduler flag for alternating tasks
    static boolean sched_flag = false;
    if (sched_flag){
        gameplay_task();
    } else {
        led_task();
    }
    sched_flag = !sched_flag;
}

/*
 * TASK 1
 */
void gameplay_task () {
    // increase global call counter
    cycle_counter++;    
    static uint32_t internal_counter = 0;
    // elapsed time, shared and overwritten by each button
    static uint32_t elapsed_time = 0;
    // t1 static variables for debouncing
    static uint32_t t1_new = 0;
    static uint32_t t1_old = 0;
    static uint32_t last_toggle_time_t1 = 0;
    static uint32_t allow_t1_change = 1;
    // t2 static variables for debouncing
    static uint32_t t2_new = 0;
    static uint32_t t2_old = 0;
    static uint32_t last_toggle_time_t2 = 0;
    static uint32_t allow_t2_change = 1;
    // t3 static variables for debouncing
    static uint32_t t3_new = 0;
    static uint32_t t3_old = 0;
    static uint32_t last_toggle_time_t3 = 0;
    static uint32_t allow_t3_change = 1;
        
    // enable t1 and t2 button simulation
    #if(SIMULATE_GAME==0)
        // check buttons
        t1_new = digitalRead(T1_PIN);
        t2_new = digitalRead(T2_PIN);
    #else
        // fake button press just before the deadline (otherwise spped would go to inf.)
        if (cycle_counter >= (CYCLES_PER_SECOND / game_speed - 2)) {
            if (prog_state & MASK_GAME_RUNNING) {
                // player #1 pressed at the end of his period
                if (!(prog_state & MASK_BALL_PASSED) && (prog_state & MASK_PLAYER_1)) {
                    t1_new = 1;
                    #if (DEBUG_PRINTS==1)
                        Serial.println("P1 SIM");
                    #endif
                }
                // player #2 pressed at the end of his period
                else if (!(prog_state & MASK_BALL_PASSED) && (prog_state & MASK_PLAYER_2)) {
                    t2_new = 1;
                    #if (DEBUG_PRINTS==1)
                        Serial.println("P2 SIM");
                    #endif
                }
            } 
        } else {
            t1_new = 0;
            t2_new = 0;
        }
    #endif
    
    // do not simulate reset button
    t3_new = digitalRead(T3_PIN);

    // debounce buttons
    if (t1_new != t1_old) {
        // change state and global button variable
        if (allow_t1_change) {
            t1 = t1_new;
            t1_old = t1_new;
            allow_t1_change = 0;
            last_toggle_time_t1 = millis();
            //Serial.print(t1);
        }
        // calc time from last press
        elapsed_time = millis() - last_toggle_time_t1;
        if (elapsed_time > DEBOUNCE_TIME_MS) {
            // allow state change on next cycle (20ms)
            allow_t1_change = 1;
        }
    } 
    if (t2_new != t2_old) {
        // change state and global button variable
        if (allow_t2_change) {
            t2 = t2_new;
            t2_old = t2_new;
            allow_t2_change = 0;
            last_toggle_time_t2 = millis();
            //Serial.print(t2);
        }
        // calc time from last press
        elapsed_time = millis() - last_toggle_time_t2;
        if (elapsed_time > DEBOUNCE_TIME_MS) {
            // allow state change on next cycle (20ms)
            allow_t2_change = 1;
        }
    }
    if (t3_new != t3_old) {
        // change state and global button variable
        if (allow_t3_change) {
            t3 = t3_new;
            t3_old = t3_new;
            allow_t3_change = 0;
            last_toggle_time_t3 = millis();
            //Serial.print(t3);
        }
        // calc time from last press
        elapsed_time = millis() - last_toggle_time_t3;
        if (elapsed_time > DEBOUNCE_TIME_MS) {
            // allow state change on next cycle (20ms)
            allow_t3_change = 1;
        }
    }   

    // Flag to stop showing winner was raised (MASK_STOP_WINNER) and
    // LED blinking interval indicating the winner has finished (MASK_SHOWING_WINNER)
    if ((prog_state & MASK_STOP_WINNER) && 
      (!(prog_state & MASK_SHOWING_WINNER))) {
        // reset program state
        stop_game();
        internal_counter = 0;
    }
    
    // Period has ended (ball at player, or in the middle of a pass)
    if (cycle_counter >= CYCLES_PER_SECOND / game_speed) {
      #if (DEBUG_PRINTS==1)
        Serial.print("Speed: ");
        Serial.println(game_speed);
      #endif
      // Pass has finished, player receives pass
      if (prog_state & MASK_BALL_PASSED) {
          change_player_after_pass();   // change ball posession
      } 
      // Game has ended, still showing winner
      else if (prog_state & MASK_SHOWING_WINNER) {
          // Speed was reset to 1.0, cycle_counter was reset to 0, 
          // so this gets called once per second
          internal_counter++;
          // After X amount of seconds, stop showing winner
          if (internal_counter >= SHOWING_WINNER_TIME_S) {
              // Let LED finish blinking interval, then stop game
              // indicate to LED, that it should stop current interval
              prog_state |= MASK_STOP_WINNER;   
          }
      }
      // End of cycle, ball still in player's posession - game over
      else if (prog_state & MASK_PLAYER_1) {
          // end of game, player 1 lost
          declare_winner(1);
      }
      else if (prog_state & MASK_PLAYER_2) {
          // end of game, player 2 lost
          declare_winner(0);
      }
      // reset cycle counter
      cycle_counter = 0;    
    }
    return;
}

/*
 * TASK 2
 */
void led_task () {
    // propagation is relative number, from 0.0 to 1.0
    float propagation = cycle_counter / (CYCLES_PER_SECOND / game_speed);

    // game started, ball passed
    if (prog_state & MASK_BALL_PASSED) {
        #if (LED_THEME==0)
            // player #1 passed the ball
            if (prog_state & MASK_PLAYER_1) {
                if (propagation < 0.25) {
                    LED_analog ( (1-propagation*2)*255, 0, 0, 0 );
                }
                else if (propagation < 0.50) {
                    LED_analog ( (1-propagation*2)*255, (1-abs(0.5-propagation)*4)*255, 0, 0 );
                }
                else if (propagation < 0.75) {
                    LED_analog ( 0, (1-abs(0.5-propagation)*4)*255, (propagation-0.5)*2*255, 0 );
                }
                else {
                    LED_analog ( 0, 0, (propagation-0.5)*2*255, 0 );
                }
            } 
            // player #2 passed the ball
            else {
                if (propagation < 0.25) {
                    LED_analog ( 0, 0, (1-propagation*2)*255, 0 );
                }
                else if (propagation < 0.50) {
                    LED_analog ( 0, (1-abs(0.5-propagation)*4)*255, (1-propagation*2)*255, 0 );
                }
                else if (propagation < 0.75) {
                    LED_analog ( (propagation-0.5)*2*255, (1-abs(0.5-propagation)*4)*255, 0, 0 );
                }
                else {
                    LED_analog ( (propagation-0.5)*2*255, 0, 0, 0 );
                }
            }
        #endif
        #if (LED_THEME==1)
            // player #1 passed the ball
            if (prog_state & MASK_PLAYER_1) {
                if (propagation < 0.333) {
                    LED_analog ( (1-propagation*3)*255, 0, 0, 0 );
                }
                else if (propagation < 0.666) {
                    LED_analog ( 0, (1-abs(0.5-propagation)*6)*255, 0, 0 );
                }
                else {
                    LED_analog ( 0, 0, (propagation-0.666)*3*255, 0 );
                }
            } 
            // player #2 passed the ball
            else {
                if (propagation < 0.33) {
                    LED_analog ( 0, 0, (1-propagation*3)*255, 0 );
                }
                else if (propagation < 0.66) {
                    LED_analog ( 0, (1-abs(0.5-propagation)*6)*255, 0, 0 );
                }
                else {
                    LED_analog ( (propagation-0.666)*3*255, 0, 0, 0 );
                }
            }
        #endif
    }  
    // pulse L1 or L3 to show winner
    else if (prog_state & MASK_SHOWING_WINNER) {
        // determine pulse direction and intensity
        win_blink_counter = win_blink_counter + win_counter_invert*40;
        if (win_blink_counter > 255) {
            win_counter_invert = -win_counter_invert;
        win_blink_counter = win_blink_counter + win_counter_invert*40;
        } else if (win_blink_counter < WIN_BLINK_RESET_VAL) {
            win_counter_invert = -win_counter_invert;
            win_blink_counter = win_blink_counter + win_counter_invert*40;
        }
        // pulsate winner
        if (prog_state & MASK_PLAYER_1) {
            LED_analog ( win_blink_counter, 0, 0, 0 );
        } else {
            LED_analog ( 0, 0, win_blink_counter, 0 );
        }
        // stop when flag is raised and pulse interval has ended (non-abrupt stop)
        if ((prog_state & MASK_STOP_WINNER) && (win_blink_counter <= WIN_BLINK_RESET_VAL)){
            prog_state &= ~MASK_SHOWING_WINNER;
        }
    }
    // if nothing of the above is showing, glow player (in possession of the ball)
    // player #1
    else if (prog_state & MASK_PLAYER_1) {
        LED_analog ( 255, 0, 0, LF_INDICATOR_VAL );
    }
    // player #2
    else if (prog_state & MASK_PLAYER_2) {
        LED_analog ( 0, 0, 255, LF_INDICATOR_VAL );
    }
    // default to pulse LF LED
    else if (prog_state == 0x00) {
        // determine pulse direction and intensity
        lf_blink_counter = lf_blink_counter + lf_counter_invert*5;
        if (lf_blink_counter > 250) {
            lf_counter_invert = -lf_counter_invert;
            lf_blink_counter = lf_blink_counter + lf_counter_invert*5;
        } else if (lf_blink_counter < LF_BLINK_RESET_VAL) {
            lf_counter_invert = -lf_counter_invert;
            lf_blink_counter = lf_blink_counter + lf_counter_invert*5;
        }
        // pulsate LF
        LED_analog ( 0, 0, 0, lf_blink_counter );
    }
    return;
}

/* set all leds together
 *  0 and 255 inside of "analogWrite()" default to "digitalWrite()"
 */
void LED_analog(uint32_t l1, uint32_t l2, uint32_t l3, uint32_t lf) {
    analogWrite(L1_PIN, l1);
    analogWrite(L2_PIN, l2);
    analogWrite(L3_PIN, l3);
    analogWrite(LF_PIN, lf);
    return;
}


void declare_winner(int winner) {
    // reset game speed to 0 for easier timekeeping while showing winner
    game_speed = 1.0;
    // reset program state and add appropriate masks
    prog_state = 0x00;
    prog_state |= MASK_SHOWING_WINNER;
    if (winner==0) {
        prog_state |= MASK_PLAYER_1;  // blink L1
        player_1_wins += 1;
        Serial.println("PLAYER 1 WON!");
    } else {
        prog_state |= MASK_PLAYER_2;  // blink L3
        player_2_wins += 1;
        Serial.println("PLAYER 2 WON!");
    }
    // show statistics
    Serial.print("\tPASSES COMPLETE\t");
    Serial.println(pass_counter);
    char score_buff[32];
    sprintf(score_buff, "\tSCORE\t%d:%d", player_1_wins, player_2_wins);
    Serial.println(score_buff);
    return;
}


void pass_the_ball() {
    // pass the ball to the oponent
    prog_state |= MASK_BALL_PASSED;
    game_speed *= 1.125;
    pass_counter++;
    cycle_counter = 0;
}


void change_player_after_pass () {
    // player #2 received the ball (#1 had passed)
    if (prog_state & MASK_PLAYER_1) {
        prog_state &= ~MASK_PLAYER_1;     // stop indicating passer
        prog_state &= ~MASK_BALL_PASSED;  // ball no longer in pass
        prog_state |= MASK_PLAYER_2;      // oposition player gains possesion
    }
    // player #1 received the ball (#2 had passed)
    else if (prog_state & MASK_PLAYER_2) {
        prog_state &= ~MASK_PLAYER_2;     // stop indicating passer
        prog_state &= ~MASK_BALL_PASSED;  // ball no longer in pass
        prog_state |= MASK_PLAYER_1;      // oposition player gains possesion
    }
    return;
}


void start_game() {
    // Read potentiometer and calculate initial game speed
    int pot_val = analogRead(POT_PIN);
    // initial speed(frequency) = 1Hz(1s) ~ 0.25Hz(4s)
    game_speed = 1 - (pot_val+1)/1024.0*0.75;
    //game_speed = 1;
    Serial.print("GAME STARTED, SPEED: ");
    Serial.println(game_speed);
    // Initiate program state
    prog_state |= MASK_GAME_RUNNING;
    
    // Flip coin for initial ball posession
    uint32_t z = random(0,300);
    //uint32_t z = rand();    // "(s)rand()" always returns the same number(?)
    #if (DEBUG_PRINTS==1)
        Serial.println(z);
    #endif
    if (z%2) {
        // p#1 'passed' first, p#2 gets initial ball posession
        prog_state |= MASK_PLAYER_1;  
    } else {
        // p#2 'passed' first, p#1 gets initial ball posession
        prog_state |= MASK_PLAYER_2;
    }
    // pass ball from the middle of the field
    prog_state |= MASK_BALL_PASSED;
    pass_counter = 0;   // reset passes
    // initiate movement (ball) in between both players
    cycle_counter = (CYCLES_PER_SECOND / game_speed)/2.0;
    //cycle_counter = 0;
    return;
}


void stop_game() {
    Serial.println("READY FOR NEW GAME");
    prog_state = 0x00;
    // reset all blink counters (always soft start LEDs)
    win_blink_counter = WIN_BLINK_RESET_VAL;
    lf_blink_counter = LF_BLINK_RESET_VAL;
    return;
}


void setup(){
    Serial.begin(9600);

    // Init random number seeding
    randomSeed(analogRead(SEED_PIN));
    //srand(time(NULL));    // "(s)rand()" always returns the same number(?)

    // Inite LEDs
    pinMode(LED_PIN,OUTPUT);
    pinMode(L1_PIN,OUTPUT);
    pinMode(L2_PIN,OUTPUT);
    pinMode(L3_PIN,OUTPUT);
    pinMode(LF_PIN,OUTPUT);
    LED_analog(0,0,0,0);

    // Init buttons
    pinMode(T1_PIN,INPUT);
    pinMode(T2_PIN,INPUT);
    pinMode(T3_PIN,INPUT);

    // Reset game
    stop_game();
    
    // Last thing to do is initialise the scheduler
    Timer1.initialize(SCHEDULER_PERIOD_USEC);
    Timer1.attachInterrupt(scheduler);
}


void loop(){
    // game running
    if (prog_state & MASK_GAME_RUNNING) {   // xxxx.xxx1
        // t1 toggled from 0 to 1, with debouncing
        if (t1) {
            // ball is not in the middle of a pass
            // player #1 in possession of the ball
            if (!(prog_state & MASK_BALL_PASSED) && 
              (prog_state & MASK_PLAYER_1)) {   // xxx0.10x1
                #if (DEBUG_PRINTS==1)
                    Serial.println("Player 1 pass");
                #endif
                pass_the_ball();
            } else {
                // pressed while in pass, or opponent's posession
                declare_winner(1);
            }
            // reset global button press
            t1 = 0;
        }
        // t2 toggled from 0 to 1, with debouncing
        if (t2) {
            // ball is not in the middle of a pass
            // player #2 in possession of the ball
            if (!(prog_state & MASK_BALL_PASSED) && 
              (prog_state & MASK_PLAYER_2)) {   // xxx0.10x1
                #if (DEBUG_PRINTS==1)
                    Serial.println("Player 2 pass");
                #endif
                pass_the_ball();
            } else {
                // pressed while in pass, or opponent's posession
                declare_winner(0);
            }
            // reset global button press
            t2 = 0;
        }
    }
    // t3 toggled from 0 to 1, with debouncing
    if (t3) {
        if (prog_state == 0x00) {
            start_game();
        } else {
            Serial.println("GAME INTERRUPTED");
            stop_game();
        }
        // reset global button press
        t3 = 0;
    }
}
